package main

import (
	"fmt"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `Add Two Numbers`
const description = `
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example 1:

Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.
`

// ListNode is provided by Leetcode
type ListNode struct {
	Val  int
	Next *ListNode
}

func convertNodeToArray(l *ListNode) []int {
	if l == nil {
		return []int{}
	}
	result := make([]int, 0)
	for curr := l; l != nil; l = l.Next {
		if curr == nil {
			continue
		}
		result = append(result, l.Val)
	}
	return result
}

func convertArrayToNode(input []int) *ListNode {
	output := &ListNode{}
	next := output
	for _, i := range input {
		next.Next = &ListNode{
			Val:  i,
			Next: nil,
		}
		next = next.Next
	}
	return output.Next
}

func convertIntToNodes(input int) *ListNode {
	output := &ListNode{}
	next := output
	stringVal := fmt.Sprintf("%d", input)
	inputReversed := reverseString(stringVal)
	for _, char := range inputReversed {
		next.Next = &ListNode{
			Val:  int(char-'0'),
			Next: nil,
		}
		next = next.Next
	}
	return output.Next
}

func reverseString(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func sumArr(input []int) int {
	res := 0
	op := 1
	for i := len(input) - 1; i >= 0; i-- {
		res += input[i] * op
		op *= 10
	}
	return res
}

func reverseIntArr(input []int) []int {
	i := 0
	j := len(input) - 1
	for i < j {
		input[i], input[j] = input[j], input[i]
		i++
		j--
	}
	return input
}

func solution(l1 *ListNode, l2 *ListNode) *ListNode {
	arr1 := convertNodeToArray(l1)
	arr2 := convertNodeToArray(l2)

	val1 := sumArr(reverseIntArr(arr1))
	val2 := sumArr(reverseIntArr(arr2))

	val := val1 + val2
	output := convertIntToNodes(val)
	return output
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := solution(nil, nil)
	golang.ShowProblemAnswer(solution)
}
