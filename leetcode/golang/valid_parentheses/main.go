package main

import (
	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `20. Valid Parentheses`
const description = `
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

    Open brackets must be closed by the same type of brackets.
    Open brackets must be closed in the correct order.

Example 1:

Input: s = "()"
Output: true

Example 2:

Input: s = "()[]{}"
Output: true

Example 3:

Input: s = "(]"
Output: false

Example 4:

Input: s = "([)]"
Output: false

Example 5:

Input: s = "{[]}"
Output: true
`

func solution(s string) bool {
	mapper := map[rune]rune{
		'}': '{',
		')': '(',
		']': '[',
	}
	stack := make([]rune, 0)
	for _, item := range s {
		if _, ok := mapper[item]; ok { // found closing bracket
			var top rune
			if len(stack) == 0 {
				top = ' '
			} else {
				top = stack[len(stack)-1]
				stack = stack[:len(stack)-1]
			}
			if mapper[item] != top {
				return false
			}
		} else {
			stack = append(stack, item)
		}
	}
	return len(stack) == 0
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := solution("")
	golang.ShowProblemAnswer(solution)
}
