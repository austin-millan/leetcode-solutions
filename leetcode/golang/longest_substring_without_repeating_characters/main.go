package main

import (
	"gitlab.com/austin-millan/leetcode-solutions"
)

// https://leetcode.com/problems/longest-substring-without-repeating-characters/discuss/1162270/GO-Using-map-(sliding-window)

const title = `Longest Substring without repeating characters`
const description = `
Given a string s, find the length of the longest substring without repeating characters.

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

Example 4:

Input: s = ""
Output: 0
`

//Idea : trying to solve this using a map
//slidingwindow : a map with the char as the key
//it has a start pointer called start and end pointer called end
//iterate through the string
//if the char is not in slidingwindow, add it to the slidingwindow and increment the value of your end pointer
//update the value of your count which will be the max number of characters in your window
//if the char is found , then remove this from the window and increment your start
//Time Complexity : O(n)
func solution(s string) int {
	slidingwindow := make(map[byte]int)
	start:=0
	end:=0
	count:=0
	for (end < len(s)) {
		//check if the char at end pointer is in the sliding window
		//if not present add it to the sliding window
		//increment the end pointer
		//update count value
		if _,ok:=slidingwindow[s[end]];!ok{
			slidingwindow[s[end]]++
			end++
			count = max(len(slidingwindow),count)
			continue

		}
		//else remove it from the sliding window and increment the value of start pointer
		delete(slidingwindow,s[start])
		start++


	}
	return count

}

func max(a,b int) int {
	if a>b{
		return a
	}
	return b
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := solution("abcabcdefg")
	golang.ShowProblemAnswer(solution)
}
