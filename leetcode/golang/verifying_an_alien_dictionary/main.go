package main

import (
	"fmt"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `953. Verifying an Alien Dictionary`
const description = `
In an alien language, surprisingly they also use english lowercase letters, but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.

Given a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are sorted lexicographicaly in this alien language.
`

func isAlienSorted(words []string, order string) bool {
	priority := charPriority(order)
	for i, word := range words {
		fmt.Printf("Word %d: %s\n", i, word)
		lastPriority := 0
		for _, char := range word {
			if prio, ok := priority[string(char)]; !ok {
				lastPriority = prio
				continue
			} else {
				if prio < lastPriority {
					fmt.Printf("prio (%d) < lastPriority (%d) for letter: %s\n", prio, lastPriority, string(char))
					return false
				}
				fmt.Printf("prio (%d) >= lastPriority (%d) for letter: %s\n", prio, lastPriority, string(char))
				lastPriority = prio
			}
		}
	}
	return true
}

func charPriority(order string) (out map[string]int) {
	out = make(map[string]int, 0)
	for k, v := range order {
		out[string(v)] = k
	}
	return
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := isAlienSorted([]string{}, "")
	golang.ShowProblemAnswer(solution)
}
