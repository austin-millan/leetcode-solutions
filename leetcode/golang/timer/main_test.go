package main

import "testing"

func Test_maxSubArray(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "nums = [-2,1,-3,4,-1,2,1,-5,4]",
			args: args{
				nums: []int{-2,1,-3,4,-1,2,1,-5,4},
			},
			want: 6,
		},
		{
			name: "nums = [1]",
			args: args{
				nums: []int{1},
			},
			want: 1,
		},
		{
			name: "nums = [0]",
			args: args{
				nums: []int{0},
			},
			want: 0,
		},
		{
			name: "nums = [-1]",
			args: args{
				nums: []int{-1},
			},
			want: -1,
		},
		{
			name: "nums = [-2147483647]",
			args: args{
				nums: []int{-2147483647},
			},
			want: -2147483647,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := timer(); got != tt.want {
				t.Errorf("maxSubArray() = %v, want %v", got, tt.want)
			}
		})
	}
}
