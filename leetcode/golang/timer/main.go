package main

import (
	"fmt"
	"time"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `TIMER STUFF`
const description = ``

func timer() int {
	timer := time.NewTimer(time.Second * 5)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-timer.C:
				fmt.Printf("Timer finished")
				done <- true
			case <- done:
				fmt.Printf("First loop told to finish")
			}
		}
	}()

	<- done
	fmt.Printf("Done!")
	return 0

}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := timer()
	golang.ShowProblemAnswer(solution)
}
