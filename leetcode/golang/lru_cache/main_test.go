package main

import (
	"testing"
)

func Test_LRUCacheOne(t *testing.T) {
	// New LRU
	lru := Constructor(1)

	lru.Put(1, 9)
	value := lru.Get(1)
	if value != 9 {
		t.Errorf("Expected 9")
	}
}

func Test_LRUCacheTwo(t *testing.T) {
	// New LRU
	lru := Constructor(2)

	lru.Put(1, 9)
	lru.Put(2, 99)
	lru.Put(3, 99)
	value := lru.Get(1)
	if value != 0 {
		t.Errorf("Expected 0")
	}
}