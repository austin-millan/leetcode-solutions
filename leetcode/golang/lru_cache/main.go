package main

import (
	"container/list"
	"fmt"

	"gitlab.com/austin-millan/leetcode-solutions"
)

/*
TODO: ListElement needs to store a pair inside Value containing key/value of a cached item.
This will allow cache to properly clean up lru.hashMap, without it, list items get evicted but the map doesn't update properly.
*/

const title = `146. LRU Cache`
const description = `
Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.

Implement the LRUCache class:

    LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
    int get(int key) Return the value of the key if the key exists, otherwise return -1.
    void put(int key, int value) Update the value of the key if the key exists. Otherwise, add the key-value pair to the cache. If the number of keys exceeds the capacity from this operation, evict the least recently used key.

Follow up:
Could you do get and put in O(1) time complexity?
`

// LRUCache TODO
type LRUCache struct {
	capacity int
	hashMap  map[int]*list.Element
	data     *list.List
}

// Constructor returns a new LRUCache
func Constructor(capacity int) LRUCache {
	return LRUCache{
		capacity: capacity,
		hashMap:  make(map[int]*list.Element, capacity),
		data:     list.New(),
	}
}

// Get fetches a value from LRUCache
func (lru *LRUCache) Get(key int) int {
	var gotElement *list.Element
	var ok bool
	if gotElement, ok = lru.hashMap[key]; !ok {
		fmt.Printf("ERROR: Key %d does not exist in map", key)
		return 0
	} else if gotElement == nil {
		fmt.Printf("ERROR: Container element for key %d does not exist", key)
		return 0
	}
	if lru.data == nil {
		fmt.Printf("ERROR: LRU list is null, needs initialization")
		return 0
	}
	lru.data.MoveToFront(gotElement)
	if converted, ok := gotElement.Value.(int); !ok {
		fmt.Printf("ERROR: Could not convert value did, did you insert non-integer?")
		return 0
	} else {
		return converted
	}
}

// Put updates a key/value in LRUCache
func (lru *LRUCache) Put(key int, value int) {
	var gotElement *list.Element
	var ok bool

	if gotElement, ok = lru.hashMap[key]; ok {
		gotElement.Value = value
		lru.data.MoveToFront(gotElement)
	} else {
		// check if full, remove node / update map if needed
		if lru.data.Len() >= lru.capacity {
			toRemove := lru.data.Back()
			if toRemove != nil {
				if toRemoveKey, ok := toRemove.Value.(int); !ok {
					fmt.Printf("ERROR: Could not convert value to integer")
					return
				} else {
					delete(lru.hashMap, toRemoveKey)
				}
			}
			removed := lru.data.Remove(toRemove)
			if val, ok := removed.(int); ok {
				fmt.Printf("Revoked value: %v", val)
			}
		}
		inserted := lru.data.PushFront(value)
		lru.hashMap[key] = inserted
	}
	return
}

func solution(capacity int) LRUCache {
	lruCache := Constructor(capacity)
	// lruCache.Put(1, 9)
	// lruCache.Put(2, 99)
	// lruCache.Put(3, 999)
	// lruCache.Put(4, 9999)
	// lruCache.Put(5, 99999)
	// lruCache.Put(6, 999999)
	return lruCache
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := solution(5)
	golang.ShowProblemAnswer(solution)
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
