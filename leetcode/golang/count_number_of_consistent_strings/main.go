package main

import (
	"strings"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `1684. Count the Number of Consistent Strings`
const description = `
You are given a string allowed consisting of distinct characters and an array of strings words. A string is consistent if all characters in the string appear in the string allowed.

Return the number of consistent strings in the array words.

Example 1:

Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
Output: 2
Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.

Example 2:

Input: allowed = "abc", words = ["a","b","c","ab","ac","bc","abc"]
Output: 7
Explanation: All strings are consistent.

Example 3:

Input: allowed = "cad", words = ["cc","acd","b","ba","bac","bad","ac","d"]
Output: 4
Explanation: Strings "cc", "acd", "ac", and "d" are consistent.
`

func unique(vals []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range vals {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func countConsistentStrings(allowed string, words []string) (count int) {
	for i := range words {
		consistent := true
		for _, wordChar := range words[i] {
			if !strings.ContainsRune(allowed, wordChar) {
				consistent = false
			}
		}
		if consistent {
			count++
		}
	}
	return
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := countConsistentStrings("ab", []string{"ad", "bd", "aaab", "baa", "badab"})
	golang.ShowProblemAnswer(solution)
}
