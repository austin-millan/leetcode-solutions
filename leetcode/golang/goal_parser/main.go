package main

import (
	"strings"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `1678. Goal Parser Interpretation`
const description = `
You own a Goal Parser that can interpret a string command. The command consists of an alphabet of "G", "()" and/or "(al)" in some order. The Goal Parser will interpret "G" as the string "G", "()" as the string "o", and "(al)" as the string "al". The interpreted strings are then concatenated in the original order.

Given the string command, return the Goal Parser's interpretation of command.

Example 1:

Input: command = "G()(al)"
Output: "Goal"
Explanation: The Goal Parser interprets the command as follows:
G -> G
() -> o
(al) -> al
The final concatenated result is "Goal".

Example 2:

Input: command = "G()()()()(al)"
Output: "Gooooal"

Example 3:

Input: command = "(al)G(al)()()G"
Output: "alGalooG"
`

var (
	grammar = map[string]string{
		"G":    "G",
		"()":   "o",
		"(al)": "al",
	}
)

func interpret(command string) string {
	for key, val := range grammar {
		command = strings.Replace(command, key, val, -1)
	}
	return command
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := interpret("G()(al)")
	golang.ShowProblemAnswer(solution)
}
