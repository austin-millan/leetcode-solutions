package main

import (
	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `53. Maximum Subarray`
const description = `
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Follow up: If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
`

func maxSubArray(nums []int) int {
	return 0
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := maxSubArray([]int{})
	golang.ShowProblemAnswer(solution)
}
