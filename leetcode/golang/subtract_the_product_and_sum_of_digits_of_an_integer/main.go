package main

import (
	"fmt"
	"strconv"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `1281. Subtract the Product and Sum of Digits of an Integer`
const description = `
Given an integer number n, return the difference between the product of its digits and the sum of its digits.

Example 1:

Input: n = 234
Output: 15 
Explanation: 
Product of digits = 2 * 3 * 4 = 24 
Sum of digits = 2 + 3 + 4 = 9 
Result = 24 - 9 = 15

Example 2:

Input: n = 4421
Output: 21
Explanation: 
Product of digits = 4 * 4 * 2 * 1 = 32 
Sum of digits = 4 + 4 + 2 + 1 = 11 
Result = 32 - 11 = 21
`

func subtractProductAndSum(n int) int {
	var (
		sum int64 = 0
		product int64 = 0
	)
	nStr := fmt.Sprintf("%d", n)
	for i, val := range nStr {
		parsedInt, err := strconv.ParseInt(string(val), 10, 32)
		if err != nil {
			continue
		}
		if i == 0 {
			sum = parsedInt
			product = parsedInt
			continue
		}
		sum += parsedInt
		product *= parsedInt
	}
	return int(product-sum)
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := subtractProductAndSum(42)
	golang.ShowProblemAnswer(solution)
}
