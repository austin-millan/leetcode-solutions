package main

import "testing"

func Test_subtractProductAndSum(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = 705",
			args: args{
				n: 705,
			},
			want: -12,
		},
		{
			name: "n = 234",
			args: args{
				n: 234,
			},
			want: 15,
		},
		{
			name: "n = 4421",
			args: args{
				n: 4421,
			},
			want: 21,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := subtractProductAndSum(tt.args.n); got != tt.want {
				t.Errorf("subtractProductAndSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
