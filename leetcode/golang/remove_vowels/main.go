package main

import (
	"strings"

	"gitlab.com/austin-millan/leetcode-solutions"
)

const title = `1119. Remove Vowels from a String`
const description = `
Given a string s, remove the vowels 'a', 'e', 'i', 'o', and 'u' from it, and return the new string.

Example 1:

Input: s = "leetcodeisacommunityforcoders"
Output: "ltcdscmmntyfrcdrs"

Example 2:

Input: s = "aeiou"
Output: ""
`

func removeVowels(s string) string {
	for _, val := range []string{"a", "e", "i", "o", "u"} {
		s = strings.Replace(s, val, "", -1)
	}
	return s
}

func main() {
	golang.ShowProblemInfo(title, description)
	solution := removeVowels("leetcodeisacommunityforcoders")
	golang.ShowProblemAnswer(solution)
}
