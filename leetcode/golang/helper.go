package golang

import (
	"fmt"
)

var (
	// TitleColor to format a title
	TitleColor = teal
	// DescriptionColor to format a description
	DescriptionColor = red
	// AnswerColor to format a answer
	AnswerColor = green
)

var (
	black   = Color("\033[1;30m%s\033[0m")
	red     = Color("\033[1;31m%s\033[0m")
	green   = Color("\033[1;32m%s\033[0m")
	yellow  = Color("\033[1;33m%s\033[0m")
	purple  = Color("\033[1;34m%s\033[0m")
	magenta = Color("\033[1;35m%s\033[0m")
	teal    = Color("\033[1;36m%s\033[0m")
	white   = Color("\033[1;37m%s\033[0m")
)

// Color a string
func Color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString,
			fmt.Sprint(args...))
	}
	return sprint
}

// ShowProblemInfo print some strings to the screen
func ShowProblemInfo(title, desc string) {
	fmt.Println(TitleColor(title))
	fmt.Println(DescriptionColor(desc))
}

// ShowProblemAnswer print some strings to the screen
func ShowProblemAnswer(answer ...interface{}) {
	for _, x := range answer {
		fmt.Println(AnswerColor(x))
	}
}
